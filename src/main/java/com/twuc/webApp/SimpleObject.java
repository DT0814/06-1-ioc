package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SimpleObject implements SimpleInterface {

    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }

    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }

    private SimpleDependent simpleDependent;


}
