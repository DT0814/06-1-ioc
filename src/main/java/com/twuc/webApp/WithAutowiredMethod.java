package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {
    private MyLogger logger;

    public WithAutowiredMethod(MyLogger logger) {
        this.logger = logger;
        logger.log("constructor");
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        logger.log("initialize");
    }
}
