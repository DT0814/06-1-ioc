package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author tao.dong
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class WithoutDependency {
    public Dependent getDependent() {
        return dependent;
    }

    private Dependent dependent;


    public WithoutDependency(Dependent dependent) {
        this.dependent = dependent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WithoutDependency)) {
            return false;
        }
        WithoutDependency that = (WithoutDependency) o;
        return Objects.equals(getDependent(), that.getDependent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDependent());
    }
}
