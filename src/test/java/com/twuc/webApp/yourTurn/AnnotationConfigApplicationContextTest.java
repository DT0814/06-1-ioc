package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import error.OutOfScanningScope;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnotationConfigApplicationContextTest {
    @Test
    void test_create_with_dependency() {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext("com.twuc.webApp");
        WithoutDependency bean = annotationConfigApplicationContext.getBean(WithoutDependency.class);
        WithoutDependency bean1 = annotationConfigApplicationContext.getBean(WithoutDependency.class);
        Assertions.assertEquals(bean,bean1);
        Assertions.assertNotNull(bean);
    }

    @Test
    void test_create_with_dependency_has_dependency() {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext("com.twuc.webApp");
        WithoutDependency bean = annotationConfigApplicationContext.getBean(WithoutDependency.class);
        Assertions.assertNotNull(bean);
        Assertions.assertNotNull(bean.getDependent());
    }

    @Test
    void test_create_out_of_scanning_scope() {
        Assertions.assertThrows(NoSuchBeanDefinitionException.class, () -> {
            AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext("com.twuc.webApp");
            OutOfScanningScope bean = annotationConfigApplicationContext.getBean(OutOfScanningScope.class);
        });
    }

    @Test
    void test_create_out_of_scanning_scope2() {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext("com.twuc.webApp");
        InterfaceImpl bean = (InterfaceImpl) annotationConfigApplicationContext.getBean(Interface.class);
        Assertions.assertNotNull(bean);
    }

    @Test
    void test_create_out_of_scanning_scope_1() {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SimpleObject bean = (SimpleObject) annotationConfigApplicationContext.getBean(SimpleInterface.class);
        Assertions.assertEquals(bean.getSimpleDependent().getName(), "O_o");
        Assertions.assertNotNull(bean);
    }
}
