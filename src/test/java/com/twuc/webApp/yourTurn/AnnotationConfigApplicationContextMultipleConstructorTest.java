package com.twuc.webApp.yourTurn;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.twuc.webApp.InterfaceWithMultipleImpls;
import com.twuc.webApp.MultipleConstructor;
import com.twuc.webApp.MyLogger;
import com.twuc.webApp.WithAutowiredMethod;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;
import java.util.Map;

@SpringBootTest
@AutoConfigureMockMvc
public class AnnotationConfigApplicationContextMultipleConstructorTest {
    AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void test_constructor() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        Assertions.assertNotNull(bean.getDependent());
        Assertions.assertNull(bean.getString());
    }
    @Test
    void test_autowired_constructor() {
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);
        MyLogger logger = context.getBean(MyLogger.class);
        Assertions.assertIterableEquals(Arrays.asList("constructor","initialize"),logger.getLines());
    }

    @Test
    void test_interface_implements() {
        Map<String, InterfaceWithMultipleImpls> beansOfType = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        Assertions.assertEquals(beansOfType.size(),3);
        Assertions.assertIterableEquals(beansOfType.keySet(),Arrays.asList("implementationA","implementationB","implementationC"));

    }
}
